#include<iostream>
#include<sstream>
#include<fstream>
#include<iomanip>
#include<sys/stat.h>
#include<cmath>
using namespace std;

// ROOT libraries
#include <TF1.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TObject.h>
#include <TEventList.h>
#include <TBranch.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGaxis.h>
#include <TMarker.h>
#include <TText.h>
#include <TMath.h>
#include <TSpectrum.h>
#include <TBox.h>
#include <TLatex.h>
#include <TString.h>
#include <TSystem.h>
#include <THStack.h>
#include "TApplication.h"

// INGRID (PM) libraries
#include "lib/INGRIDEVENTSUMMARY.h"
#include "lib/IngridHitSummary.h"
#include "lib/IngridSimHitSummary.h"
#include "lib/IngridSimVertexSummary.h"
#include "lib/IngridSimParticleSummary.h"
#include "lib/BeamInfoSummary.h"
#include "lib/IngridBasicReconSummary.h"

#include <getopt.h>

bool drawhists = true;
double default_cos_cut = 0;

int main(int argc, char **argv){

  TApplication * app = new TApplication("app", &argc, argv);
  cout<< "Welcome" << endl;
  cout<< "Opening Events" << endl;

  TChain * tree = new TChain("tree");
  tree->Add("data/PM_MC_Beam1_BirksCorrected_wNoise_ana.root");

  int get_opt;
  char *cvalue = NULL;
  while ((get_opt= getopt (argc, argv, "ac:")) != -1){
    switch(get_opt){
      case 'a':
        tree->Add("data/PM_MC_Beam2_BirksCorrected_wNoise_ana.root");
        tree->Add("data/PM_MC_Beam3_BirksCorrected_wNoise_ana.root");
        tree->Add("data/PM_MC_Beam4_BirksCorrected_wNoise_ana.root");
        tree->Add("data/PM_MC_Beam5_BirksCorrected_wNoise_ana.root");
        tree->Add("data/PM_MC_Beam6_BirksCorrected_wNoise_ana.root");
        tree->Add("data/PM_MC_Beam7_BirksCorrected_wNoise_ana.root");
        tree->Add("data/PM_MC_Beam8_BirksCorrected_wNoise_ana.root");
        tree->Add("data/PM_MC_Beam9_BirksCorrected_wNoise_ana.root");
        tree->Add("data/PM_MC_Beam10_BirksCorrected_wNoise_ana.root");
        break;
      case 'c':
        cvalue = optarg;
        drawhists = false;
        default_cos_cut = atof(cvalue);
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
             fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                  "Unknown option character `\\x%x'.\n",
                  optopt);
          return 1;
    }
  }

  int nevt = (int) tree->GetEntries();
  cout<< "Total Number Of Events=" << nevt << endl;

  IngridEventSummary* evt = new IngridEventSummary();
  TBranch * Br = tree->GetBranch("fDefaultReco.");
  Br->SetAddress(&evt);
  tree->SetBranchAddress("fDefaultReco.", &evt);
  IngridHitSummary * Hit = new IngridHitSummary();
  IngridSimHitSummary * Sim_Hit = new IngridSimHitSummary();

  //THStack *hs = new THStack("hs","hit_pe");
  TH1F * hit_pe_pion = new TH1F("hit_pe_pion", "hit_pe", 100, 0, 400);
    hit_pe_pion->SetLineColor(kRed);
    hit_pe_pion->SetMarkerStyle(21);
    hit_pe_pion->SetMarkerColor(kRed);
  TH1F * hit_pe_mu = new TH1F("hit_pe_mu", "hit_pe", 100, 0, 400);
    hit_pe_mu->SetLineColor(kGreen);
    hit_pe_mu->SetMarkerStyle(21);
    hit_pe_mu->SetMarkerColor(kGreen);
  TH1F * hit_pe_p = new TH1F("hit_pe_p", "hit_pe", 100, 0, 400);
    hit_pe_p->SetLineColor(kBlue);
    hit_pe_p->SetMarkerStyle(21);
    hit_pe_p->SetMarkerColor(kBlue);

  THStack *mcont_short = new THStack("mcont_short","muon_cont_hist short");
  TH1F * muon_cont_short = new TH1F("muon_cont_short", "muon_cont_hist short", 60, 0, 1);
    muon_cont_short->SetFillColor(kGreen);
    muon_cont_short->SetMarkerStyle(21);
    muon_cont_short->SetMarkerColor(kGreen);

  TH1F * muon_cont_short_b = new TH1F("muon_cont_short_b", "muon_cont_hist short", 60, 0, 1);
    muon_cont_short_b->SetFillColor(kRed);
    muon_cont_short_b->SetMarkerStyle(21);
    muon_cont_short_b->SetMarkerColor(kRed);

  mcont_short->Add(muon_cont_short_b);
  mcont_short->Add(muon_cont_short);

  THStack *mcont_long = new THStack("mcont_long","muon_cont_hist long");
  TH1F * muon_cont_long = new TH1F("muon_cont_long", "muon_cont_hist long", 60, 0, 1);
    muon_cont_long->SetFillColor(kGreen);
    muon_cont_long->SetMarkerStyle(21);
    muon_cont_long->SetMarkerColor(kGreen);
  TH1F * muon_cont_long_b = new TH1F("muon_cont_long_b", "muon_cont_hist long", 60, 0, 1);
    muon_cont_long_b->SetFillColor(kRed);
    muon_cont_long_b->SetMarkerStyle(21);
    muon_cont_long_b->SetMarkerColor(kRed);

  mcont_long->Add(muon_cont_long_b);
  mcont_long->Add(muon_cont_long);


  THStack *avg_pe_stackplot = new THStack("mcont_short", "avg. p_e per track");
	  TH1F * muon_pe = new TH1F ("muon_pe", "avg. p_e per track", 30, 0, 60);
 	  TH1F * proton_pe = new TH1F ("proton_pe", "avg. p_e per track", 30, 0, 60);
  avg_pe_stackplot->Add(proton_pe);
  avg_pe_stackplot->Add(muon_pe);

  TGraph * track_energy_loss_m = new TGraph();
  TGraph * track_energy_loss_p = new TGraph();
  
  TGraph * mcont_cut;
  TGraph * tracks_eff_cut;

  int sim_muon_hits_pertrack = 0;
  int rec_muon_hits_pertrack = 0;
  int sim_tot_hits_pertrack = 0;

  int sim_protons_hits_pertrack = 0;

  int sim_muon_tracks = 0;
  int okay_muon_tracks = 0;

  double magic_cos_cut = 0.05;
  if (default_cos_cut) magic_cos_cut = default_cos_cut;
  cout << "cos. cut value: " << magic_cos_cut << endl;

  double track1_length = 0;
  double track2_length = 0;
  double track1_cont = 0;
  double track2_cont = 0;
  int track1_simp = 0;
  int track2_simp = 0;
    
  for(int ievt = 0; ievt < nevt; ievt++){ 
  //loop over INGRID event (ingridsimvertex if MC, integration cycle of 580ns if data)
   if( (ievt%100) == 0 ) cout<< "Processing " << ievt << endl;
    evt->Clear();
    tree->GetEntry(ievt);
    //IngridSimVertexSummary * simver = (IngridSimVertexSummary*)(evt->GetSimVertex(0));

  int NIngBasRec = evt->NPMAnas();
  if (NIngBasRec!=1) continue;
  for(int irec = 0; irec < NIngBasRec; irec++){
      PMAnaSummary * recon = (PMAnaSummary*) evt->GetPMAna(irec);
      bool veto = recon->vetowtracking;
      bool edge = recon->edgewtracking;
      if( !(veto || edge) ) break;

      int nTracks = recon->Ntrack;
      //cout << "# reconstructed tracks: " << nTracks << endl;
      int nsimp = evt->NIngridSimParticles();
      //cout << "# sim. particles: " << nsimp << endl;
      //if(simver->inttype != 1) break;
      if (nTracks > 2) break;

      multimap <float, float> zhit_energy_map;
      multimap <float, float>::iterator iter;
      for(int itrk = 0; itrk < nTracks; itrk++){
        float avg_pe_per_track = 0;
        int which_particle = 0;
        //int nhit_ts = recon->NhitTs(itrk);
        double z_min = recon->GetIngridHitTrk(0, itrk)->z;
        double z_max = recon->GetIngridHitTrk(0, itrk)->z;
        vector<float> angle_z = recon->angle;
        vector<float> theta_x = recon->thetax;
        vector<float> theta_y = recon->thetay;

        for(int ihit = 0; ihit < recon->NhitTs(itrk); ihit++){
          Hit = recon->GetIngridHitTrk(ihit, itrk);
          //if ( Hit->mod != 16) break;
          //if ( Hit->z > 84) break;

          //cout << "z: " << Hit->z << "\tpe: " << Hit->pe << endl;
          zhit_energy_map.insert( 
                                  make_pair(Hit->z, Hit->pe)
                                );
          int n_sim_hits = Hit->NSimHits();
          if(!n_sim_hits) break;
          if(n_sim_hits > 1) cout << "WOW!!!" << endl;

          /*    first and last track hit    */
          if ( Hit->z < z_min ){
            z_min = Hit->z;
          }

          if ( Hit->z > z_max ){
            z_max = Hit->z;
          }
          /*-------------------------------*/
          for(int isim = 0; isim < n_sim_hits; isim++){
            Sim_Hit = Hit->GetIngridSimHit(isim);
            if (Sim_Hit->pdg == 13) {
              sim_muon_hits_pertrack++;
            }
            else if (Sim_Hit->pdg == 2212){
              sim_protons_hits_pertrack++;
            }
            sim_tot_hits_pertrack++;
            //cout << "simhit pdg: " << Sim_Hit->pdg << endl;
            switch(Sim_Hit->pdg){
            case 13: // mu-
              hit_pe_mu->Fill(Hit->pe);
              break;
            case 2212:
              hit_pe_p->Fill(Hit->pe);
              break;
            case 211: // pi+
              hit_pe_pion->Fill(Hit->pe);
              break;
            case -211: // pi-
              hit_pe_pion->Fill(Hit->pe);
              break;
            default:
              break;
            }
          }
        }

      double tracklength = (z_max - z_min) / TMath::Cos( angle_z[itrk] * TMath::Pi()/180 );

      //cout << "mapsize: " << zhit_energy_map.size() << endl;
      //cout << "nhits: " << recon->NhitTs(itrk) << endl;
      for( iter = zhit_energy_map.begin(); iter != zhit_energy_map.end(); ++iter ){
        //cout << "hit z: " << iter->first << "\thit pe: " << iter->second << endl;

       /* if ((itrk==1) && (ievt == 20)){ track_energy_loss_m->SetPoint(
                                                        recon->NhitTs(1),
                                                        iter->first,
                                                        iter->second
                                                      );
                          break;
        }*/

        if ( ((iter->first - z_min) / ( z_max - z_min ) > 0.8) || ((iter->first - z_min) / ( z_max - z_min ) < 0.2))
        	if (iter->second > 10)
           		avg_pe_per_track += iter->second;
      }

      double proj1 = TMath::Tan( theta_x[itrk] * TMath::Pi()/180 ) * TMath::Cos( angle_z[itrk] * TMath::Pi()/180 );
      double proj2 = TMath::Tan( theta_y[itrk] * TMath::Pi()/180 ) * TMath::Cos( angle_z[itrk] * TMath::Pi()/180 );
      double proj3 = TMath::Cos( angle_z[itrk] * TMath::Pi()/180 );
      double norm = TMath::Sqrt( proj1 * proj1 + proj2 * proj2 + proj3 * proj3 );

      proj1 /= norm;
      proj2 /= norm;
      proj3 /= norm;

        for( int particle = 0; particle < nsimp; particle++){
          IngridSimParticleSummary * simparticle = (IngridSimParticleSummary*)(evt->GetSimParticle(particle));
          //if(
           //  (simparticle->pdg == 13)
           //   || 
           //  (simparticle->pdg == 2212)
            //){

            if (simparticle->pdg == 13) sim_muon_tracks++;

            if  (
                   (TMath::Abs(simparticle->dir[2] - proj3) < magic_cos_cut) // 3D angle
                   &&
                   (TMath::Abs(simparticle->dir[0] - proj2) < magic_cos_cut) // thetaY
                   &&
                   (TMath::Abs(simparticle->dir[1] - proj1) < magic_cos_cut) // thetaX
                  )
              {
              rec_muon_hits_pertrack = recon->NhitTs(itrk);
              okay_muon_tracks++;
              if (rec_muon_hits_pertrack != 0){
                double muon_cont = (double) sim_muon_hits_pertrack / rec_muon_hits_pertrack;
                if ( nTracks == 1 ){ if (simparticle->pdg == 13) muon_cont_long->Fill(muon_cont);
                                     else muon_cont_long_b->Fill(muon_cont);
                                     //which_particle = 13;
                                     //which_particle = simparticle->pdg;
                }                                    
                else if (nTracks == 2){
                  if(track1_length == 0){
                    track1_length = tracklength;
                    track1_cont = muon_cont;
                    track1_simp = simparticle->pdg;
                    //if (track1_cont > 0.8) which_particle = 13;
                    //else which_particle = 2212;
                    which_particle = simparticle->pdg;
                  }
                  else{
                    track2_length = tracklength;
                    track2_cont = muon_cont;
                    track2_simp = simparticle->pdg;
                    //if (track2_cont > 0.8) which_particle = 13;
                    //else which_particle = 2212;
                    which_particle = simparticle->pdg;
                  
                    if(track1_length > track2_length){
                      //cout << "1: " << track1_length << endl;
                      //cout << "2: " << track2_length << endl;
                      if (track1_simp == 13) muon_cont_long->Fill(track1_cont);
                      else muon_cont_long_b->Fill(track1_cont);
                      if (track2_simp == 13) muon_cont_short->Fill(track2_cont);
                      else muon_cont_short_b->Fill(track2_cont);
                    }
                    else{

                      if (track2_simp == 13) muon_cont_long->Fill(track2_cont);
                      else muon_cont_long_b->Fill(track2_cont);
                      if (track1_simp == 13) muon_cont_short->Fill(track1_cont);
                      else muon_cont_short_b->Fill(track1_cont);
                    }
                  }
                }

                sim_muon_hits_pertrack = 0;
                rec_muon_hits_pertrack = 0;   
              }
            }
          //}
        }

        avg_pe_per_track = avg_pe_per_track / recon->NhitTs(itrk); // Average # of photoelectrons in track per hit
        switch(which_particle){
          case 13: // mu-
            muon_pe->Fill(avg_pe_per_track);
            break;
          case 2212:
            proton_pe->Fill(avg_pe_per_track);
            break;   
          default:
            break;
        }

        which_particle = 0;
        zhit_energy_map.clear();
      //trk loop
      }

      track1_length = 0;
      track2_length = 0;
    }
  }

  cout << "sim_muon_tracks: " << sim_muon_tracks << endl;
  cout << "okay_muon_tracks: " << okay_muon_tracks << endl;

  double scale_mu = hit_pe_mu->GetEntries();
  double scale_p = hit_pe_p->GetEntries();
  double scale_pion = hit_pe_pion->GetEntries();

  hit_pe_mu->Scale(1/scale_mu);
  hit_pe_p->Scale(1/scale_p);
  hit_pe_pion->Scale(1/scale_pion);

  double avg_cont_long = muon_cont_long->GetMean();
  double muon_tracks_eff = (double) okay_muon_tracks / sim_muon_tracks;

  cout << "long track average muon hit contamination: "<< avg_cont_long << endl;
  cout << "muon track matching efficiency: " << muon_tracks_eff << endl;

  TFile * mcont = new TFile("mcont.root", "UPDATE");
  mcont_cut = (TGraph*) mcont->Get("mcont_cut");
  if (!mcont_cut) mcont_cut = new TGraph();
  mcont_cut->GetXaxis()->SetTitle("cos cut value");
  mcont_cut->GetYaxis()->SetTitle("true muon hits");
  mcont_cut->SetPoint(mcont_cut->GetN(), magic_cos_cut, avg_cont_long);
  mcont_cut->Draw("AC*");
  mcont_cut->Write("mcont_cut");
  mcont->Close();

  TFile * tracks_eff = new TFile("tracks_eff.root", "UPDATE");
  tracks_eff_cut = (TGraph*) tracks_eff->Get("tracks_eff_cut");
  if (!tracks_eff_cut) tracks_eff_cut = new TGraph();
  tracks_eff_cut->GetXaxis()->SetTitle("cos cut value");
  tracks_eff_cut->GetYaxis()->SetTitle("muon tracks eff");
  tracks_eff_cut->SetPoint(tracks_eff_cut->GetN(), magic_cos_cut, muon_tracks_eff);
  tracks_eff_cut->Draw("AC*");
  tracks_eff_cut->Write("tracks_eff_cut");
  tracks_eff->Close();

if (drawhists){
  TCanvas* c1 = new TCanvas("c1", "hit_pe", 800, 600);
  TCanvas* c2 = new TCanvas("c2", "muon hits contamination", 800, 600);
  TCanvas* c3 = new TCanvas("c3", "muon hits contamination", 800, 600);
  TCanvas* c4 = new TCanvas("c4", "atata atatat tata", 800, 600);
  TCanvas* c5 = new TCanvas("c5", "akvpospoaskev-2bjas", 800, 600);

  c1->Divide(1,1);
  c2->Divide(1,1);
  c3->Divide(1,1);
  c4->Divide(1,1);
  c5->Divide(1,1);

  c1->cd(1);
  TLegend* leg = new TLegend(1.0,0.5,0.995,0.4);
  leg->AddEntry(hit_pe_p, "protons");
  leg->AddEntry(hit_pe_mu, "mu-");
  leg->AddEntry(hit_pe_pion, "pi+, pi-");
  hit_pe_mu->Draw();
  hit_pe_pion->Draw("SAME");
  hit_pe_p->Draw("SAME");
  leg->Draw("SAME");
  hit_pe_pion->GetXaxis()->SetTitle("Charge (e)");
  hit_pe_pion->GetYaxis()->SetTitle("# of events");

  c2->cd(1);
  //muon_cont_short->Draw();
  //muon_cont_short->GetXaxis()->SetTitle("muon cont.");
  //muon_cont_short->GetYaxis()->SetTitle("# of hits");

  mcont_short->Draw();
  mcont_short->GetHistogram()->GetXaxis()->SetTitle("muon cont.");
  mcont_short->GetHistogram()->GetYaxis()->SetTitle("# of hits");
  mcont_short->Draw();

  c3->cd(1);
  //muon_cont_long->Draw();
  //muon_cont_long->GetXaxis()->SetTitle("muon cont.");
  //muon_cont_long->GetYaxis()->SetTitle("# of hits");

  mcont_long->Draw();
  mcont_long->GetHistogram()->GetXaxis()->SetTitle("muon cont.");
  mcont_long->GetHistogram()->GetYaxis()->SetTitle("# of hits");
  mcont_long->Draw();


  int mu_tracks = 0;
  int p_tracks = 0;
  int mu_tracks_after = 0;
  int last_bin = 9;
  for (int i = 1; i <= last_bin ; i++){
  	mu_tracks += muon_pe->GetBinContent(i);
  	p_tracks += proton_pe->GetBinContent(i);
  }

  for (int i = 1; i <= 30 ; i++){
  	mu_tracks_after += muon_pe->GetBinContent(i);
  }

  double purity = (double) mu_tracks / (p_tracks + mu_tracks) ;
  cout << "2-track sample muon identification purity: "
       << purity << endl;

  double efficiency = (double) mu_tracks / mu_tracks_after ;
  cout << "2-track sample muon identification efficiency: "
       << efficiency << endl;


  muon_pe->Scale((double)1/muon_pe->GetEntries());
  proton_pe->Scale((double)1/proton_pe->GetEntries());

  c4->cd(1);
  muon_pe->SetLineColor(kBlue);
  proton_pe->SetLineColor(kRed);
  muon_pe->Draw();
  proton_pe->Draw("SAME");

  c5->cd(1);
  avg_pe_stackplot->Draw();
  //track_energy_loss_m->Draw("AP*");
  //c5->cd(2);
  //track_energy_loss_p->Draw("AP*");
}

if (drawhists){
  app->Run();
  hit_pe_p->Delete();
  hit_pe_mu->Delete();
  hit_pe_pion->Delete();
  mcont_short->Delete();
  mcont_long->Delete();
  muon_pe->Delete();
  proton_pe->Delete();
  track_energy_loss_m->Delete();
  track_energy_loss_p->Delete();
  avg_pe_stackplot->Delete();
}

  return 0;
}
 
