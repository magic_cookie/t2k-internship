#include<iostream>
#include<sstream>
#include<fstream>
#include<iomanip>
#include<sys/stat.h>
#include<cmath>
using namespace std;

// ROOT libraries
#include <TF1.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TObject.h>
#include <TEventList.h>
#include <TBranch.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGaxis.h>
#include <TMarker.h>
#include <TText.h>
#include <TMath.h>
#include <TSpectrum.h>
#include <TBox.h>
#include <TLatex.h>
#include <TString.h>
#include <TSystem.h>
#include <THStack.h>
#include "TApplication.h"

// INGRID (PM) libraries
#include "lib/INGRIDEVENTSUMMARY.h"
#include "lib/IngridHitSummary.h"
#include "lib/IngridSimHitSummary.h"
#include "lib/IngridSimVertexSummary.h"
#include "lib/IngridSimParticleSummary.h"
#include "lib/BeamInfoSummary.h"
#include "lib/IngridBasicReconSummary.h"

double opening_angle(double ang1, double thetax1, double thetay1, double ang2, double thetax2, double thetay2){
  float Track1[3];
  float Track2[3];
  double oangle;

  Track1[0]=TMath::Tan(thetax1 * TMath::Pi()/180)*TMath::Cos(ang1 * TMath::Pi()/180);
  Track1[1]=TMath::Tan(thetay1* TMath::Pi()/180 )*TMath::Cos(ang1 * TMath::Pi()/180);
  Track1[2]=TMath::Cos(ang1 * TMath::Pi()/180);
  double NormTrack1=TMath::Sqrt(Track1[0]*Track1[0]+Track1[1]*Track1[1]+Track1[2]*Track1[2]);

  Track2[0]=TMath::Tan(thetax2 * TMath::Pi()/180)*TMath::Cos(ang2 * TMath::Pi()/180);
  Track2[1]=TMath::Tan(thetay2 * TMath::Pi()/180)*TMath::Cos(ang2 * TMath::Pi()/180);
  Track2[2]=TMath::Cos(ang2 * TMath::Pi()/180);
  double NormTrack2=TMath::Sqrt(Track2[0]*Track2[0]+Track2[1]*Track2[1]+Track2[2]*Track2[2]);

  for(int i=0;i<3;i++){
    //cout<<i<<": (";
    Track1[i]/=NormTrack1;
    Track2[i]/=NormTrack2;
    //cout<< Track1[i]<<", "<< Track2[i]<<endl;
  }

  oangle = TMath::ACos(Track1[0]*Track2[0]+Track1[1]*Track2[1]+Track1[2]*Track2[2]);

  return oangle;
}

int main(int argc, char **argv)
{

  TApplication * app = new TApplication("app", &argc, argv);

  cout<< "Welcome" << endl;

  cout<< "Opening Events" << endl;

  TChain * tree = new TChain("tree");
  tree->Add("data/PM_MC_Beam1_BirksCorrected_wNoise_ana.root");
  if ( (argc == 2) && !strcmp( argv[1], "all" ) ){
  	tree->Add("data/PM_MC_Beam2_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam3_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam4_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam5_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam6_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam7_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam8_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam9_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam10_BirksCorrected_wNoise_ana.root");
  }

  int nevt = (int) tree->GetEntries();
  
  cout<< "Total Number Of Events=" << nevt << endl;
   
  IngridEventSummary* evt = new IngridEventSummary();
  TBranch * Br = tree->GetBranch("fDefaultReco.");
  Br->SetAddress(&evt);
  tree->SetBranchAddress("fDefaultReco.", &evt);
  IngridHitSummary * Hit = new IngridHitSummary();
  IngridSimHitSummary * SimHit = new IngridSimHitSummary();


  //Hists
  TH1F * typehist = new TH1F("typehist", "type of event", 59, 1, 60);
  TH1F * reco_typehist = new TH1F("reco_typehist", "reconstructed type of event", 59, 1, 60);
  TH1F * rec_num_tracks = new TH1F("rec_num_tracks", "# of reconstructed tracks", 10, 0, 10);
  TH1F * vertex_pe = new TH1F("vertex_pe", "vertex photoelectrons", 100, 0, 100);
  TH2F * stopping_power = new TH2F("stopping_power", "lkjlh", 1000, 0, 1, 50, 0, 500 );
  TProfile * sp = new TProfile("sp", "charge deposit", 1000, 0, 1, 0, 1000);
  TH2F * sim_sp = new TH2F("sim_sp", "sim charge deposit", 100, 0, 200 , 0 ,1000);
  TH1F * two_track_angle = new TH1F("two_track_angle", "2-track opening angle", 30, 0, 180);



  THStack *hs = new THStack("hs","before applying the cuts");
  TH1F * rec_CC1P_tracks = new TH1F("rec_CC1P_tracks", "CC1P", 8, 0, 8);
    rec_CC1P_tracks->SetFillColor(kGreen);
    rec_CC1P_tracks->SetMarkerStyle(21);
    rec_CC1P_tracks->SetMarkerColor(kGreen);
  TH1F * rec_CCQE_tracks = new TH1F("rec_CCQE_tracks", "# of reconstructed tracks", 8, 0, 8);
    rec_CCQE_tracks->SetFillColor(kBlue);
    rec_CCQE_tracks->SetMarkerStyle(21);
    rec_CCQE_tracks->SetMarkerColor(kBlue);
  TH1F * rec_CCother_tracks = new TH1F("rec_CCother_tracks", "# of reconstructed tracks", 8, 0, 8);
    rec_CCother_tracks->SetFillColor(kRed);
    rec_CCother_tracks->SetMarkerStyle(21);
    rec_CCother_tracks->SetMarkerColor(kRed);

  hs->Add(rec_CC1P_tracks);
  hs->Add(rec_CCQE_tracks);
  hs->Add(rec_CCother_tracks);

  THStack *hs_new = new THStack("hs_new", "after applying the cuts");
  TH1F * rec_CC1P_tracks_new = new TH1F("rec_CC1P_tracks_new", "# of reconstructed tracks", 8, 0, 8);
    rec_CC1P_tracks_new->SetFillColor(kGreen);
    rec_CC1P_tracks_new->SetMarkerStyle(21);
    rec_CC1P_tracks_new->SetMarkerColor(kGreen);
  TH1F * rec_CCQE_tracks_new = new TH1F("rec_CCQE_tracks_new", "# of reconstructed tracks", 8, 0, 8);
    rec_CCQE_tracks_new->SetFillColor(kBlue);
    rec_CCQE_tracks_new->SetMarkerStyle(21);
    rec_CCQE_tracks_new->SetMarkerColor(kBlue);
  TH1F * rec_CCother_tracks_new = new TH1F("rec_CCother_tracks_new", "# of reconstructed tracks", 8, 0, 8);
    rec_CCother_tracks_new->SetFillColor(kRed);
    rec_CCother_tracks_new->SetMarkerStyle(21);
    rec_CCother_tracks_new->SetMarkerColor(kRed);

  hs_new->Add(rec_CC1P_tracks_new);
  hs_new->Add(rec_CCQE_tracks_new);
  hs_new->Add(rec_CCother_tracks_new);


  THStack *hsa = new THStack("hsa", "1-track angular");
  TH1F * rec_CC1P_tracks_a = new TH1F("rec_CC1P_tracks_new", "# of reconstructed tracks", 90, 0, 90);
    rec_CC1P_tracks_a->SetFillColor(kGreen);
    rec_CC1P_tracks_a->SetMarkerStyle(21);
    rec_CC1P_tracks_a->SetMarkerColor(kGreen);
  TH1F * rec_CCQE_tracks_a = new TH1F("rec_CCQE_tracks_new", "# of reconstructed tracks", 90, 0, 90);
    rec_CCQE_tracks_a->SetFillColor(kBlue);
    rec_CCQE_tracks_a->SetMarkerStyle(21);
    rec_CCQE_tracks_a->SetMarkerColor(kBlue);
  TH1F * rec_CCother_tracks_a = new TH1F("rec_CCother_tracks_new", "# of reconstructed tracks", 90, 0, 90);
    rec_CCother_tracks_a->SetFillColor(kRed);
    rec_CCother_tracks_a->SetMarkerStyle(21);
    rec_CCother_tracks_a->SetMarkerColor(kRed);

  hsa->Add(rec_CC1P_tracks_a);
  hsa->Add(rec_CCQE_tracks_a);
  hsa->Add(rec_CCother_tracks_a);


  THStack *hs_charge = new THStack("hs_charge", "vertex charge deposit");
  TH1F * rec_CC1P_tracks_charge = new TH1F("rec_CC1P_tracks_charge", "# of reconstructed tracks", 200, 0, 6000);
    rec_CC1P_tracks_charge->SetFillColor(kGreen);
    rec_CC1P_tracks_charge->SetMarkerStyle(21);
    rec_CC1P_tracks_charge->SetMarkerColor(kGreen);
  TH1F * rec_CCQE_tracks_charge = new TH1F("rec_CCQE_tracks_charge", "# of reconstructed tracks", 200, 0, 6000);
    rec_CCQE_tracks_charge->SetFillColor(kBlue);
    rec_CCQE_tracks_charge->SetMarkerStyle(21);
    rec_CCQE_tracks_charge->SetMarkerColor(kBlue);
  TH1F * rec_CCother_tracks_charge = new TH1F("rec_CCother_tracks_charge", "# of reconstructed tracks", 200, 0, 6000);
    rec_CCother_tracks_charge->SetFillColor(kRed);
    rec_CCother_tracks_charge->SetMarkerStyle(21);
    rec_CCother_tracks_charge->SetMarkerColor(kRed);

  hs_charge->Add(rec_CC1P_tracks_charge);
  hs_charge->Add(rec_CCQE_tracks_charge);
  hs_charge->Add(rec_CCother_tracks_charge);


  THStack *hs_oangle = new THStack("hs_oangle", "2-track opening angle ");
  TH1F * rec_CC1P_tracks_oangle = new TH1F("rec_CC1P_tracks_oangle", "# of reconstructed tracks", 30, 0, 180);
    rec_CC1P_tracks_oangle->SetFillColor(kGreen);
    rec_CC1P_tracks_oangle->SetMarkerStyle(21);
    rec_CC1P_tracks_oangle->SetMarkerColor(kGreen);
  TH1F * rec_CCQE_tracks_oangle = new TH1F("rec_CCQE_tracks_oangle", "# of reconstructed tracks", 30, 0, 180);
    rec_CCQE_tracks_oangle->SetFillColor(kBlue);
    rec_CCQE_tracks_oangle->SetMarkerStyle(21);
    rec_CCQE_tracks_oangle->SetMarkerColor(kBlue);
  TH1F * rec_CCother_tracks_oangle = new TH1F("rec_CCother_tracks_oangle", "# of reconstructed tracks", 30, 0, 180);
    rec_CCother_tracks_oangle->SetFillColor(kRed);
    rec_CCother_tracks_oangle->SetMarkerStyle(21);
    rec_CCother_tracks_oangle->SetMarkerColor(kRed);

  hs_oangle->Add(rec_CC1P_tracks_oangle);
  hs_oangle->Add(rec_CCQE_tracks_oangle);
  hs_oangle->Add(rec_CCother_tracks_oangle);
    
  int nf;

  for(int ievt = 0; ievt < nevt; ievt++){ 
  //loop over INGRID event (ingridsimvertex if MC, integration cycle of 580ns if data)
   if( (ievt%100) == 0 ) cout<< "Processing " << ievt << endl;
    evt->Clear();
    tree->GetEntry(ievt);
    IngridSimVertexSummary * simver = (IngridSimVertexSummary*)(evt->GetSimVertex(0));
    int event_type = simver->inttype;
  //cout<<"Type of Interaction="<< simver->inttype <<endl;  
  //cout<< "xnu" << simver->xnu << endl;
  //cout<< "neutrino type: " << simver->nutype << endl;
    typehist->Fill(event_type);


    /*int nsimp = evt->NIngridSimParticles();
    cout << "nsimparticles: " << nsimp << endl;

    for( int particle = 0; particle < nsimp; particle++){
      IngridSimParticleSummary * simparticle = (IngridSimParticleSummary*)(evt->GetSimParticle(particle));
        cout << "trackid: " << simparticle->trackid << endl;
        cout << "parentid: " << simparticle->parentid << endl;
        cout << "pdg: " << simparticle->pdg << endl;
        cout << "Energy: " << simparticle->momentum[0] << endl;
        cout << "momentum1: " << simparticle->momentum[1] << endl;
        cout << "momentum2: " << simparticle->momentum[2] << endl;
        cout << "momentum3: " << simparticle->momentum[3] << endl;
        cout << "tracklength: " << simparticle->length << endl;
	}*/


  int NIngBasRec = evt->NPMAnas();

  for(int irec = 0; irec < NIngBasRec; irec++){
  //loop on reconstruction: considered as same event => same time cluster, vertex reconstructed...
      PMAnaSummary * recon = (PMAnaSummary*) evt->GetPMAna(irec);
      bool veto = recon->vetowtracking;
      bool edge = recon->edgewtracking;
      if( !(veto || edge) ) break;
      int nTracks = recon->Ntrack;
      reco_typehist->Fill(event_type);
      rec_num_tracks->Fill(nTracks);

      switch( event_type ) {
      case 1:
          rec_CCQE_tracks->Fill(nTracks);
          break;
      case 11:
          rec_CC1P_tracks->Fill(nTracks);
          break;
      case 12:
          rec_CC1P_tracks->Fill(nTracks);
          break;
      case 13:
          rec_CC1P_tracks->Fill(nTracks);
          break;     
      default:
          rec_CCother_tracks->Fill(nTracks);
          break;    
      }


      if (nTracks == 1) {
        //cout<< "ntracks: " << nTracks << endl;
        vector<float> angle_3 = recon->angle;
        //one_track_angular->Fill(angle_3[0]);
        //cout<< "3D angle: " << angle_3[0] << endl;
        switch( event_type ) {
        case 1:
          //if ( angle_3[0] < 40 )
          rec_CCQE_tracks_a->Fill(angle_3[0]);
          break;
        case 11:
          //if ( (40 <= angle_3[0]) && (angle_3[0] < 70) )
          rec_CC1P_tracks_a->Fill(angle_3[0]);
          break;
        case 12:
          //if ( (40 <= angle_3[0]) && (angle_3[0] < 70) )
          rec_CC1P_tracks_a->Fill(angle_3[0]);
          break;
        case 13:
          //if ( (40 <= angle_3[0]) && (angle_3[0] < 70) )
          rec_CC1P_tracks_a->Fill(angle_3[0]);
          break;     
        default:
          //if ( angle_3[0] >= 70 )
          rec_CCother_tracks_a->Fill(angle_3[0]);
          break;    
        }
      }
      else if (nTracks == 2) {
        double ang1 = recon->angle[0];
        double thetax1 = recon->thetax[0];
        double thetay1 = recon->thetay[0];
        double ang2 = recon->angle[1];
        double thetax2 = recon->thetax[1];
        double thetay2 = recon->thetay[1];
        double oangle = opening_angle(ang1, thetax1, thetay1, ang2, thetax2, thetay2) * 180/TMath::Pi();
        //cout << "nTracks: " << nTracks << endl;
        //cout << "opening angle" << oangle << endl;
        //two_track_angle->Fill(oangle);
        switch( event_type ) {
        case 1:
          rec_CCQE_tracks_oangle->Fill(oangle);
          break;
        case 11:
          rec_CC1P_tracks_oangle->Fill(oangle);
          break;
        case 12:
          rec_CC1P_tracks_oangle->Fill(oangle);
          break;
        case 13:
          rec_CC1P_tracks_oangle->Fill(oangle);
          break;     
        default:
          rec_CCother_tracks_oangle->Fill(oangle);
          break;    
        }
      }

      vector<int> plane_x = recon->startxpln;
      vector<int> plane_y = recon->startypln;
      vector<int> lastplane_x = recon->endxpln;

      //cout << "mod" << recon->mod << endl;
     //if (nTracks == 1){
      for(int itrk = 0; itrk < nTracks; itrk++){
        //cout<< "number of hits in this track: " << recon->NhitTs(itrk) << endl;
        double vertex_charge_deposit;
        //vector<float> angle_z = recon->angle;
        for(int ihit = 0; ihit < recon->NhitTs(itrk); ihit++){
           Hit = recon->GetIngridHitTrk(ihit, itrk);
           if ( Hit->mod != 16) break;
           if ( Hit->z > 84) break;
           double length = lastplane_x[itrk] - plane_x[itrk] ;
           stopping_power->Fill( (Hit->pln - plane_x[itrk]) / length / 18 , Hit->pe);
           //cout << "startxpln" << plane_x[itrk] << endl;
           //cout << "endxpln" << recon->endxpln[itrk] << endl;
           sp->Fill( (Hit->pln - plane_x[itrk]) / length / 18, Hit->pe, 1 );
           //vertex_pe->Fill(Hit->z);
           //cout<< "z:\t" << Hit->z << "xy: " << Hit->xy << "view: " << Hit->view << endl;
           //cout<< "e:\t" << Hit->pe << endl; 
           if ( Hit->pln - plane_x[itrk] <= 2 ) vertex_charge_deposit += Hit->pe;
           //cout<< "Hit charge=" << Hit->pe << ", Hit view=" << Hit->view
           //<< ", Hit xy position=" << Hit->xy <<", Hit z position="<< Hit->z <<endl;
           }
        switch( event_type ) {
        case 1:
          rec_CCQE_tracks_charge->Fill(vertex_charge_deposit);
          break;
        case 11:
          rec_CC1P_tracks_charge->Fill(vertex_charge_deposit);
          break;
        case 12:
          rec_CC1P_tracks_charge->Fill(vertex_charge_deposit);
          break;
        case 13:
          rec_CC1P_tracks_charge->Fill(vertex_charge_deposit);
          break;     
        default:
          rec_CCother_tracks_charge->Fill(vertex_charge_deposit);
          break;    
        }

    if ( (vertex_charge_deposit > 1000) && (vertex_charge_deposit < 2000)  )
    switch( event_type ) {
    case 1:
       rec_CCQE_tracks_new->Fill(nTracks);
       break;
    case 11:
       rec_CC1P_tracks_new->Fill(nTracks);
       nf += 1;
       break;
    case 12:
       rec_CC1P_tracks_new->Fill(nTracks);
       nf += 1;
       break;
    case 13:
       rec_CC1P_tracks_new->Fill(nTracks);
       nf += 1;
       break;     
    default:
       rec_CCother_tracks_new->Fill(nTracks);
       break;    
    }
        //vertex_pe->Fill(vertex_charge_deposit);
        vertex_charge_deposit = 0;
      }
    }
  }

  double eff_CC1P;
  double pur_CC1P;

  int num_pur = rec_CC1P_tracks_new->GetEntries();
  int denom_pur = rec_CCQE_tracks_new->GetEntries()
                + rec_CCother_tracks_new->GetEntries()
                + rec_CC1P_tracks_new->GetEntries();

  pur_CC1P = (double) num_pur / denom_pur ;

  eff_CC1P = (double) nf / rec_CC1P_tracks_charge->GetEntries() ;

  cout << "CC1P purity: \t" << pur_CC1P << endl;
  cout << "CC1P efficiency: \t" << eff_CC1P << endl;

  //Canvas
  TCanvas* c1 = new TCanvas("c1", "First canvas", 800, 600);
  TCanvas* c2 = new TCanvas("c2", "event content", 800, 600);
  TCanvas* c3 = new TCanvas("c3", "1-track angular", 800, 600);
  TCanvas* c4 = new TCanvas("c4", "vertex_charge_deposit", 800, 600);
  TCanvas* c5 = new TCanvas("c5", "stopping power", 800, 600);
  TCanvas* c6 = new TCanvas("c6", "2-track opening angle", 800, 600);
  TCanvas* c7 = new TCanvas("c7", "nTracks", 800, 600);
  c1->Divide(1,1);
  c2->Divide(2,1);
  c3->Divide(1,1);
  c4->Divide(1,1);
  c5->Divide(2,1);
  c6->Divide(1,1);
  c7->Divide(1,1);

  c1->cd(1);
  typehist->SetFillColor(30);
  typehist->GetXaxis()->SetTitle("event type code");
  typehist->GetYaxis()->SetTitle("# of events");
  typehist->Draw();
  reco_typehist->SetFillColor(4);
  reco_typehist->Draw("SAME");
  c7->cd(1);
  rec_num_tracks->GetXaxis()->SetTitle("number of tracks");
  rec_num_tracks->GetYaxis()->SetTitle("# of events");
  rec_num_tracks->Draw();

  c2->cd(1);
  TLegend* leg = new TLegend(1.0,0.5,0.995,0.4);
  leg->AddEntry(rec_CCQE_tracks,"CCQE");
  leg->AddEntry(rec_CC1P_tracks,"CC1P");
  leg->AddEntry(rec_CCother_tracks,"CCother");
  hs->Draw();
  leg->Draw();
  hs->GetXaxis()->SetTitle("number of tracks");
  hs->GetYaxis()->SetTitle("# of events");
  c2->cd(2);
  TLegend* leg1 = new TLegend(1.0,0.5,0.995,0.4);
  leg1->AddEntry(rec_CCQE_tracks_new, "CCQE");
  leg1->AddEntry(rec_CC1P_tracks_new, "CC1P");
  leg1->AddEntry(rec_CCother_tracks_new, "CCother");
  hs_new->Draw();
  leg1->Draw();
  hs_new->GetXaxis()->SetTitle("number of tracks");
  hs_new->GetYaxis()->SetTitle("# of events");

  c3->cd(1);
  TLegend* leg2 = new TLegend(1.0,0.5,0.995,0.4);
  leg2->AddEntry(rec_CCQE_tracks_a, "CCQE");
  leg2->AddEntry(rec_CC1P_tracks_a, "CC1P");
  leg2->AddEntry(rec_CCother_tracks_a, "CCother");
  hsa->Draw();
  leg2->Draw();
  hsa->GetXaxis()->SetTitle("Angle [deg]");

  c4->cd(1);
  TLegend* leg4 = new TLegend(1.0,0.5,0.995,0.4);
  leg4->AddEntry(rec_CCQE_tracks_charge, "CCQE");
  leg4->AddEntry(rec_CC1P_tracks_charge, "CC1P");
  leg4->AddEntry(rec_CCother_tracks_charge, "CCother");
  hs_charge->Draw();
  leg4->Draw();
  hs_charge->GetXaxis()->SetTitle("Charge [e]");

  c5->cd(1);
  stopping_power->Draw();
  //vertex_pe->Draw();
  c5->cd(2);
  sp->Draw();

  c6->cd(1);
  TLegend* leg3 = new TLegend(1.0,0.5,0.995,0.4);
  leg3->AddEntry(rec_CCQE_tracks_oangle, "CCQE");
  leg3->AddEntry(rec_CC1P_tracks_oangle, "CC1P");
  leg3->AddEntry(rec_CCother_tracks_oangle, "CCother");
  hs_oangle->Draw();
  leg3->Draw();
  hs_oangle->GetXaxis()->SetTitle("Angle [deg]");
  //two_track_angle->Draw();

  app->Run();
  typehist->Delete();
  rec_num_tracks->Delete();
  leg->Delete();
  hs->Delete();
  hs_new->Delete();
  hsa->Delete();
  hs_charge->Delete();
  sp->Delete();
  stopping_power->Delete();
  hs_oangle->Delete();
  //vertex_pe->Delete();
  return 0;
}
 
