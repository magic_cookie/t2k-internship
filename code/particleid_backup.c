#include<iostream>
#include<sstream>
#include<fstream>
#include<iomanip>
#include<sys/stat.h>
#include<cmath>
using namespace std;

// ROOT libraries
#include <TF1.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TObject.h>
#include <TEventList.h>
#include <TBranch.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGaxis.h>
#include <TMarker.h>
#include <TText.h>
#include <TMath.h>
#include <TSpectrum.h>
#include <TBox.h>
#include <TLatex.h>
#include <TString.h>
#include <TSystem.h>
#include <THStack.h>
#include "TApplication.h"

// INGRID (PM) libraries
#include "lib/INGRIDEVENTSUMMARY.h"
#include "lib/IngridHitSummary.h"
#include "lib/IngridSimHitSummary.h"
#include "lib/IngridSimVertexSummary.h"
#include "lib/IngridSimParticleSummary.h"
#include "lib/BeamInfoSummary.h"
#include "lib/IngridBasicReconSummary.h"

int main(int argc, char **argv){

  TApplication * app = new TApplication("app", &argc, argv);

  cout<< "Welcome" << endl;

  cout<< "Opening Events" << endl;

  TChain * tree = new TChain("tree");
  tree->Add("data/PM_MC_Beam1_BirksCorrected_wNoise_ana.root");
  if ( (argc == 2) && !strcmp( argv[1], "all" ) ){
  	tree->Add("data/PM_MC_Beam2_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam3_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam4_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam5_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam6_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam7_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam8_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam9_BirksCorrected_wNoise_ana.root");
  	tree->Add("data/PM_MC_Beam10_BirksCorrected_wNoise_ana.root");
  }

  int nevt = (int) tree->GetEntries();
  
  cout<< "Total Number Of Events=" << nevt << endl;
   
  IngridEventSummary* evt = new IngridEventSummary();
  TBranch * Br = tree->GetBranch("fDefaultReco.");
  Br->SetAddress(&evt);
  tree->SetBranchAddress("fDefaultReco.", &evt);
  IngridHitSummary * Hit = new IngridHitSummary();
  IngridSimHitSummary * Sim_Hit = new IngridSimHitSummary();

  //THStack *hs = new THStack("hs","hit_pe");
  TH1F * hit_pe_pion = new TH1F("hit_pe_pion", "hit_pe", 100, 0, 400);
    hit_pe_pion->SetLineColor(kRed);
    hit_pe_pion->SetMarkerStyle(21);
    hit_pe_pion->SetMarkerColor(kRed);
  TH1F * hit_pe_mu = new TH1F("hit_pe_mu", "hit_pe", 100, 0, 400);
    hit_pe_mu->SetLineColor(kGreen);
    hit_pe_mu->SetMarkerStyle(21);
    hit_pe_mu->SetMarkerColor(kGreen);
  TH1F * hit_pe_p = new TH1F("hit_pe_p", "hit_pe", 100, 0, 400);
    hit_pe_p->SetLineColor(kBlue);
    hit_pe_p->SetMarkerStyle(21);
    hit_pe_p->SetMarkerColor(kBlue);

  TH1F * hit_pe_pion2 = new TH1F("hit_pe_pion2", "avg. pe per track", 100, 0, 400);
    hit_pe_pion2->SetLineColor(kRed);
    hit_pe_pion2->SetMarkerStyle(21);
    hit_pe_pion2->SetMarkerColor(kRed);
  TH1F * hit_pe_mu2 = new TH1F("hit_pe_mu2", "avg. pe per track", 100, 0, 400);
    hit_pe_mu2->SetLineColor(kGreen);
    hit_pe_mu2->SetMarkerStyle(21);
    hit_pe_mu2->SetMarkerColor(kGreen);
  TH1F * hit_pe_p2 = new TH1F("hit_pe_p2", "avg. pe per track", 100, 0, 400);
    hit_pe_p2->SetLineColor(kBlue);
    hit_pe_p2->SetMarkerStyle(21);
    hit_pe_p2->SetMarkerColor(kBlue);

  TH1F * muon_cont_short = new TH1F("muon_cont_short", "muon_cont_hist short", 60, 0, 1);
  TH1F * muon_cont_long = new TH1F("muon_cont_long", "muon_cont_hist long", 60, 0, 1);

  int sim_muon_hits_pertrack = 0;
  int rec_muon_hits_pertrack = 0;
  int sim_tot_hits_pertrack = 0;

  int sim_muon_tracks = 0;
  int okay_muon_tracks = 0;

  double magic_cos_cut = 0.08;

  double track1_length = 0;
  double track2_length = 0;
  double track1_cont = 0;
  double track2_cont = 0;
    
  for(int ievt = 0; ievt < nevt; ievt++){ 
  //loop over INGRID event (ingridsimvertex if MC, integration cycle of 580ns if data)
   if( (ievt%100) == 0 ) cout<< "Processing " << ievt << endl;
    evt->Clear();
    tree->GetEntry(ievt);
    IngridSimVertexSummary * simver = (IngridSimVertexSummary*)(evt->GetSimVertex(0));

  int NIngBasRec = evt->NPMAnas();
  if ( NIngBasRec!=1) continue;
  for(int irec = 0; irec < NIngBasRec; irec++){
      PMAnaSummary * recon = (PMAnaSummary*) evt->GetPMAna(irec);
      bool veto = recon->vetowtracking;
      bool edge = recon->edgewtracking;
      if( !(veto || edge) ) break;

      int nTracks = recon->Ntrack;
      //cout << "# reconstructed tracks: " << nTracks << endl;
      int nsimp = evt->NIngridSimParticles();
      //cout << "# sim. particles: " << nsimp << endl;
      //if(simver->inttype != 1) break;
      if (nTracks > 2) break;


      double first_track_length = 0;
      double first_muon_cont = 0;

      for(int itrk = 0; itrk < nTracks; itrk++){
        float avg_pe_per_track = 0;
        int which_particle = 1000000;

        double z_min = recon->GetIngridHitTrk(0, itrk)->z;
        double z_max = recon->GetIngridHitTrk(0, itrk)->z;
        vector<float> angle_z = recon->angle;
        vector<float> theta_x = recon->thetax;
        vector<float> theta_y = recon->thetay;

        for(int ihit = 0; ihit < recon->NhitTs(itrk); ihit++){
          Hit = recon->GetIngridHitTrk(ihit, itrk);
          //if ( Hit->mod != 16) break;
          //if ( Hit->z > 84) break;

          int n_sim_hits = Hit->NSimHits();
          if(!n_sim_hits) break;
          if(n_sim_hits > 1) cout << "WOW!!!" << endl;

          /*    first and last track hit    */

          if ( Hit->z < z_min ){
            z_min = Hit->z;
          }

          if ( Hit->z > z_max ){
            z_max = Hit->z;
          }
          /*-------------------------------*/

          float photoelectrons = Hit->pe; //THIS
          avg_pe_per_track += photoelectrons;
          for(int isim = 0; isim < n_sim_hits; isim++){
            Sim_Hit = Hit->GetIngridSimHit(isim);
            which_particle = Sim_Hit->pdg;
            //cout << recon->NhitTs(itrk) <<" "<< which_particle << endl;
            if (Sim_Hit->pdg == 13) sim_muon_hits_pertrack++;
            sim_tot_hits_pertrack++;
            //cout << "simhit pdg: " << Sim_Hit->pdg << endl;
            switch(Sim_Hit->pdg){
            case 13: // mu-
              hit_pe_mu->Fill(photoelectrons);
              break;
            case 2212:
              hit_pe_p->Fill(photoelectrons);
              break;
            case 211: // pi+
              hit_pe_pion->Fill(photoelectrons);
              break;
            case -211: // pi-
              hit_pe_pion->Fill(photoelectrons);
              break;    
            default:
              break;    
            }
          }              
        }

      double tracklength = (z_max - z_min) / TMath::Cos( angle_z[itrk] * TMath::Pi()/180 );
      double proj1 = TMath::Tan( theta_x[itrk] * TMath::Pi()/180 ) * TMath::Cos( angle_z[itrk] * TMath::Pi()/180 );
      double proj2 = TMath::Tan( theta_y[itrk] * TMath::Pi()/180 ) * TMath::Cos( angle_z[itrk] * TMath::Pi()/180 );
      double proj3 = TMath::Cos( angle_z[itrk] * TMath::Pi()/180 );
      double norm = TMath::Sqrt( proj1 * proj1 + proj2 * proj2 + proj3 * proj3 );

      proj1 /= norm;
      proj2 /= norm;
      proj3 /= norm;

        for( int particle = 0; particle < nsimp; particle++){
          IngridSimParticleSummary * simparticle = (IngridSimParticleSummary*)(evt->GetSimParticle(particle));
          //if(
           //  (simparticle->pdg == 13)
           //   || 
           //  (simparticle->pdg == 2212)
            //){

            sim_muon_tracks++;

            if  (
                   (TMath::Abs(simparticle->dir[2] - proj3) < magic_cos_cut) // 3D angle
                   &&
                   (TMath::Abs(simparticle->dir[0] - proj2) < magic_cos_cut) // thetaY
                   &&
                   (TMath::Abs(simparticle->dir[1] - proj1) < magic_cos_cut) // thetaX
                  )
              {
              rec_muon_hits_pertrack = recon->NhitTs(itrk);
              okay_muon_tracks++;
              if (rec_muon_hits_pertrack != 0){
                double muon_cont = (double) sim_muon_hits_pertrack / rec_muon_hits_pertrack;
                if ( nTracks == 1 ) muon_cont_long->Fill(muon_cont);
                else if (nTracks == 2){
                  if(track1_length == 0){
                    track1_length = tracklength;
                    track1_cont = muon_cont;
                    //cout << "1o: " << track1_length << endl;
                  }
                  else{
                    track2_length = tracklength;
                    track2_cont = muon_cont;
                    //cout << "2o: " << track2_length << endl;
                  
                    if(track1_length > track2_length){
                      //cout << "1: " << track1_length << endl;
                      //cout << "2: " << track2_length << endl;
                      muon_cont_long->Fill(track1_cont);
                      muon_cont_short->Fill(track2_cont);
                    }
                    else{
                      muon_cont_long->Fill(track2_cont);
                      muon_cont_short->Fill(track1_cont);
                    }
                  }
                }

                sim_muon_hits_pertrack = 0;
                rec_muon_hits_pertrack = 0;   
              }
            }
          //}
        }



        avg_pe_per_track = avg_pe_per_track / recon->NhitTs(itrk); // Average # of photoelectrons in track per hit
        switch(which_particle){
          case 13: // mu-
            hit_pe_mu2->Fill(avg_pe_per_track);
            break;
          case 2212:
            hit_pe_p2->Fill(avg_pe_per_track);
            break;
          case 211: // pi+
            hit_pe_pion2->Fill(avg_pe_per_track);
            break;
          case -211: // pi-
            hit_pe_pion2->Fill(avg_pe_per_track);
            break;    
          default:
            break;
        }

      //trk loop
      }

      first_track_length = 0;
      track1_length = 0;
      track2_length = 0;
    }
  }

  cout << "sim_muon_tracks: " << sim_muon_tracks << endl;
  cout << "okay_muon_tracks: " << okay_muon_tracks << endl;

  double scale_mu = hit_pe_mu->GetEntries();
  double scale_p = hit_pe_p->GetEntries();
  double scale_pion = hit_pe_pion->GetEntries();

  hit_pe_mu->Scale(1/scale_mu);
  hit_pe_p->Scale(1/scale_p);
  hit_pe_pion->Scale(1/scale_pion);

  double scale_mu2 = hit_pe_mu2->GetEntries();
  double scale_p2 = hit_pe_p2->GetEntries();
  double scale_pion2 = hit_pe_pion2->GetEntries();

  hit_pe_mu2->Scale(1/scale_mu2);
  hit_pe_p2->Scale(1/scale_p2);
  hit_pe_pion2->Scale(1/scale_pion2);

  //TCanvas* c1 = new TCanvas("c1", "hit_pe", 800, 600);
  //TCanvas* c2 = new TCanvas("c2", "avg. pe per track", 800, 600);
  TCanvas* c3 = new TCanvas("c3", "muon hits contamination", 800, 600);
  TCanvas* c4 = new TCanvas("c4", "muon hits contamination", 800, 600);

  //c1->Divide(1,1);
  //c2->Divide(1,1);
  c3->Divide(1, 1);
  c4->Divide(1, 1);

  /*c1->cd(1);
  TLegend* leg = new TLegend(1.0,0.5,0.995,0.4);
  leg->AddEntry(hit_pe_p, "protons");
  leg->AddEntry(hit_pe_mu, "mu-");
  leg->AddEntry(hit_pe_pion, "pi+, pi-");
  //hit_pe_mu->Draw();
  //hit_pe_pion->Draw("SAME");
  //hit_pe_p->Draw("SAME");
  //leg->Draw("SAME");
  hit_pe_pion->GetXaxis()->SetTitle("Charge (e)");
  hit_pe_pion->GetYaxis()->SetTitle("# of events");

  c2->cd(1);
  TLegend* leg2 = new TLegend(1.0,0.5,0.995,0.4);
  leg2->AddEntry(hit_pe_p2, "protons");
  leg2->AddEntry(hit_pe_mu2, "mu-");
  leg2->AddEntry(hit_pe_pion2, "pi+, pi-");
  //hit_pe_mu2->Draw();
  //hit_pe_pion2->Draw("SAME");
  //hit_pe_p2->Draw("SAME");
  //leg2->Draw("SAME");
  hit_pe_pion2->GetXaxis()->SetTitle("Charge (e)");
  hit_pe_pion2->GetYaxis()->SetTitle("# of events");*/

  c3->cd(1);
  muon_cont_short->Draw();
  muon_cont_short->GetXaxis()->SetTitle("muoon cont.");
  muon_cont_short->GetYaxis()->SetTitle("# of hits");

  c4->cd(1);
  muon_cont_long->Draw();
  muon_cont_long->GetXaxis()->SetTitle("muoon cont.");
  muon_cont_long->GetYaxis()->SetTitle("# of hits");


  app->Run();
  hit_pe_p->Delete();
  hit_pe_mu->Delete();
  hit_pe_pion->Delete();
  hit_pe_p2->Delete();
  hit_pe_mu2->Delete();
  hit_pe_pion2->Delete();
  //leg->Delete();
  //leg2->Delete();
  muon_cont_short->Delete();
  muon_cont_long->Delete();
  return 0;
}
 
