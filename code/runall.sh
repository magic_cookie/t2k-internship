#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

a=($(seq 0.01 0.03 1.57 ))
length=${#a[@]}

for i in $(seq 0 $length)
do
	cut[$i+1]=${a[$i]}
done

for i in $(seq 1 $length)
do
	echo -e "\e[32mcos. cut value: ${cut[$i]} \e[0m"
	$DIR/particleid -a -c ${cut[$i]}
done

exit 0